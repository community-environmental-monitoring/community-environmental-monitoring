Community Environmental Monitoring website
========================================

This is the source code for the Community Environmental Monitoring website.

The site uses [Eleventy](https://www.11ty.io/), and [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/)
to compile and deploy the resource files into the website.

## Contributing and updating resources

See [the CONTRIBUTING.md file](https://gitlab.com/equivalentideas/community-environmental-monitoring/blob/master/CONTRIBUTING.md).

## Configuration

See the `_data/metadata.json` file.

* `repositoryFilesUrl`, *explain here how to add an edit link to posts*.

## Development

### Dependencies

The posts and templates and compiled into the website using [Eleventy](https://www.11ty.io/docs/usage/), a node.js based static site generator.

Once you have Node installed, you can install all the dependencies for this
project by running `npm install` in the project directory.

### Compiling the site

The posts and templates and compiled into the website using [Eleventy](https://www.11ty.io/docs/usage/).

To build the site into the `public/` folder, run:

```
npx eleventy
```

Eleventy can watch for file changes and serve the site to a local development
server, with:

```
npx eleventy --watch
```

Run with debug output

```
DEBUG=Eleventy* npx eleventy --serve
```

## Deployment
