---
layout: layouts/resource.njk
showHomeLink: true
postTitle: Submission to NSW Draft Clean Air Strategy
description: NSW air pollution strategy is under consideration in the NSW Draft Clean Air Strategy. This is Sydney-based group Community Environmental Montoring's submission.
date: 2021-04-20
---

## What is CEM?

[Community Environmental Monitoring (CEM)](/) is a new community grassroots organisation that aims to empower communities to respond to air quality and other environmental issues through the installation and management of an open source environmental monitoring network. We aim to produce robust data and information through analysis of our own data and other publicly available data and to communicate it in ways that are useful and accessible. We are committed to engaging with communities in their diversity, including with First Nations custodians of the environments in which we engage. 

Our group includes scientists, IT specialists, programmers,  journalists, designers, artists and other interested community members. Our aim is to collaborate with a range of partners including community and research organisations. 

We are a registered organisation that was founded in 2020. We currently have approximately 50 low cost monitors installed in NSW, most of which are in Sydney. These monitors measure PM 2.5, PM 10, humidity and temperature. We are part of a global Luftdaten network that was initiated in Stuttgart and now has more than 14,000 citizen science monitors around the world. These can be viewed on mobile devices. 

In this submission, we have drawn on research reports and preliminary observations of our own monitors, DPIE monitors and Transurban (Australian standard) monitors that are located along private motorway routes or proposed routes. Our views also reflect our own experiences as community members in Sydney.
 
## Introduction

The NSW Clean Air Strategy draft document states that the NSW government aims to achieve ongoing reductions in the impacts of air pollution on the people of New South Wales (NSW), supporting liveable communities, healthy environments and the NSW economy. 
 
These are desirable goals. However, although the strategy document describes key sources of pollution in NSW, it conveys very little about the pathways it will use to reduce emissions and protect communities. Instead the strategy is mostly based on an assumption that a continuation of current practices and policies will deliver the stated goals. The document lacks any reference to practical plans to overcome existing deficiencies in current policies or a sense of timeframes for reductions or how progress will be assessed.  There are no targets, performance indicators or ways of evaluating success. Even a discussion of how this will be formulated after the strategy is finalised is missing. 

## Lack of serious up-to-date consultation

Work on this Strategy plan began in 2016. It was released several years later than originally expected. It is hard not to conclude that this reflects a lack of priority attached to the strategy and that it has been rushed as a result of the need to respond to the NSW Parliamentary Inquiry into the health impacts of bushfires and drought on air quality which reported its findings in September, 2020. Despite these delays, only a few weeks has been allowed for public consultation and there has been little publicity about the draft. This can only reinforce concerns that the government is not serious about community consultation or working with the community on air quality. 

In fact, there has been a scientific consensus for decades that climate change will lead to an increase in fire risk due to longer fire seasons and more extreme fires. The dangers to community health have long been predicted. Experience with poor air quality on hazard-burn days must have alerted the NSW government to the health risk not just for those directly in the line of fire but also for urban populations impacted by smoke. It has been predicted that days conducive to extreme bushfires will increase by 20-50 % in South East Australia. (There are many sources for this including the submission of the Centre for Air Pollution, Energy and Health cited at page 63 of the Parliamentary Report.) Hazardous air during severe dust storms during previous droughts must surely have led to consideration of how vulnerable communities will be impacted by increasing frequency and severity of drought that will accompany a warming climate.  

One would hope that a government with all this in mind would treat strategies to prevent hazardous air quality eventuating in the future as an urgent issue. The costs in lives and sickness to public health are well known and documented. Instead, the NSW EPA has been under-resourced, and the issue has not been prioritised by the Department of Planning Industry and Environment (DPIE). 

As a result of delays, the strategy relies on previous consultations that are now somewhat out of date, and do not account for major new infrastructure, approved plans for coal mines or continuing approvals for poorly designed urban developments.  Quantitative trend data is included up to 2019 but there is little in the rest of the document to suggest that the strategy will enable the government to protect the community from long term impacts of climate change on health.  

## Detailed description of existing practice with only vague commitments for the future 

The draft makes assertions and references to other documents that are not explained. 

There is a large amount of detailed descriptive information in the first half of the document. While this provides information to the community, it is not matched by a similar engagement with future strategic directions.

A crucial goal for any government that intends to protect the community against deteriorating air quality is the reduction of fossil fuel emissions, without which the health impacts of climate change will become intolerable in future decades.  

It is stated that the strategy is complementary to the Net Zero Plan Stage 1: 2020 - 2030, which it refers to as the foundation for NSW’s action on climate change and goal to reduce the state’s emissions by 2050, and to other key NSW Government strategies and that it will be delivered and reviewed over the same timeframe as the Net Zero Plan (p.7).  

The Net Zero Plan Stage 1 may have positive benefits but its emphasis is on ‘coal innovation’ and there is only one reference in the document to air quality. Meanwhile the NSW government has continued to approve coal mines. In the Upper Hunter Valley, proposals for new coal projects have a combined output of 98 million tonnes per year, equivalent to ten new Adani-sized mines. The coal industry is currently [proposing 23 new coal mines and mine extensions in NSW](https://australiainstitute.org.au/post/ten-new-adani-mines-worth-of-coal-proposed-for-upper-hunter/). There is no indication that the NSW government intends to call a halt to further expansion. Yet each new mine and expansion has the capacity to damage the health of those in the immediate area including children yet to be born. It also continues to add to the risks of millions of NSW citizens by failing to act urgently on climate change.

It is unacceptable that a strategy document that communicates an acceptance of the need to reduce fossil fuels and claims that it is based on a ‘whole of government’ approach does not deal with these contradictions or the reality on the ground. 

The disappointment expressed by community health organisations in the Hunter Valley is understandable.  For many years, the Hunter community including its GPs, have been concerned about the impact of poor air quality on their communities. They continue to be concerned. Given that this document is supposed to set the parameters for a future NSW government approach, CEM is alarmed that Deputy Premier John Barilaro has recently accused environmental groups of distorting air quality data. (Guardian Australia 5/4/2021) There is no evidence of this. 

Community organisations and health professionals have been quoted as saying that the draft Clean Air strategy is a major disappointment. Jocelyn McGarity, Newcastle-based lawyer for Environmental Justice Australia has said 

> "NSW communities suffering health impacts from exposure to toxic air pollution have waited five years for a Clean Air Strategy that, as it stands, will do little to reduce pollution. The draft Clean Air Strategy does not contain firm commitments with measurable deadlines for delivery. It's heavy on buzzword statements that suggest the government is "developing" or "reviewing" or will "work to" and "explore" achieving certain things. These statements lack detail, are non-committal and do not provide certainty to the community that firm actions have been created that will be carried through[b]."  ( Media release) 

and:

> "In the five years the community has waited for this strategy, coal-fired power stations in NSW have caused approximately 2385 premature deaths, 37,910 asthma symptoms experienced by children and young adults, and 2250 babies to be born with low birth weight.

> The NSW government's draft Clean Air Strategy does nothing to tackle the state's largest source of pollution – coal-fired power stations. It could easily have mandated that power stations install basic pollution controls required in most other regions, including the United States, European Union, and China, and cut toxic pollutants by more than 85 percent."  (Singleton Argus, 21/3/2021)

The strategy does acknowledge that coal mining will “decline in the long term due to the global transition to lower carbon sources of energy” and that the “NSW Government is building economically resilient regional communities that can transition to new economic opportunities”. However much of what is promised is simply a continuation of strategies already in place. In other words, more of the same is being promised. These strategies have not so far  resulted in marked improvements in air quality in the Hunter region, which is nearly always poorer than the rest of NSW aside from parts of South West Sydney. 

Given the central role of fossil fuel emissions in contributing to periods of very poor air quality and the long term predictions for increasing heat, bushfires and drought, one would have expected that issue to be at the heart of the strategy. Instead, community members wanting to make submissions are expected to locate and read other plans to see how the NSW government plans to reduce fossil fuel emissions and how that will impact on communities that currently experience the worst pollution in NSW. 

We support Environment Justice Australia’s disappointment and rejection of the plan and support their strategy of People’s Action Plans (see more of this below).

## Inadequate response to NSW Parliamentary Inquiry into health impacts of exposure to poor air quality recommendations

The NSW Parliamentary Inquiry into health impacts of exposure to poor levels of air quality as a result of bushfires and drought was initially concerned that the NSW Clean Air strategy document had been put on hold altogether. It reported that it was  “reassured” that the Department did intend to deliver it. The Department claims to have taken into account the Committee’s recommendations. However the Committee recommended that the strategy should incorporate a strong framework for regulation of air pollution from industry, vehicles and wood heaters, link to a comprehensive plan for air quality monitoring across the state, and be supported by adequate resourcing of the agency responsible for implementation.

CEM considers that the draft strategy fails to adequately discuss how the framework for regulation can be strengthened. It provides no ‘comprehensive plan for air quality monitoring across the state’ and in fact has indicated in its response to the Committee that this will be done later. 

In its communications, the Department emphasises that NSW has the biggest air monitoring network in Australia. This is true but NSW is the largest state, has a huge capital city and major regional cities near fossil fuel intensive industries. There are still many areas that are inadequately covered, especially by PM 2.5 monitors.

We understand that the NSW EPA staff are currently stretched with limited resources to investigate all pollution complaints in a timely way or put in place all the monitors they would like to operate. However there is no indication of an intention by the government to fill resource gaps or strengthen the EPA’s role in the state’s planning decisions. If the NSW government fails to provide sufficient resources to government departments and agencies and support for local councils and other community organisations to do the necessary work,  it clearly will not achieve its stated aims of achieving healthy environments for all its citizens. 

In other words, the Strategy does not meet the recommendations of the Committee. 

## Strategy understates bushfire and drought air quality impacts


The plan does reveal that air quality in NSW has overall deteriorated in recent years and that these trends may continue as the impacts of climate change continue to be experienced. 

*“Climate change is likely to result in changes to air pollution episodes, which could be characterised by high pollutant levels lasting up to several days and which could extend over wider areas. Air quality in our cities is also under pressure from unprecedented growth.”*

Given the seriousness of this statement and its implications for future air quality, it is not sufficiently explored in the rest of the document. Rather, even this statement seems to minimise the urgency of this issue.

In fact, as a search of the DPIE’s own data shows, poor air quality continued not just for a few days, but for weeks and even months in some areas of NSW. For example, in three months at Bringelly in Sydney between late October 2019 and late January 2020, for more than 30 days, the air quality exceeded the national daily standards of 25 PM 2.5 ugm-3. 

The NSW Annual Air Quality Statement 2019 reported that of 35 of 36 sites that measured PM 2.5 in 2019, only one site at Narrabri recorded an annual average that met the national standard of 8 ugm-3 for PM 2.5. In other words, no site in the Greater Metropolitan area of Sydney where 5 million people live met the national standard for PM 2.5.

27 of 33 sites recorded averages above 10 ugm-3 PM 2.5. The worst annual average of 17.2, which was more than 100% above the national standard, was at Armidale which was impacted by both wood fires and bushfires. This was a record since monitoring began.

Climate scientists have warned for more than twenty years that climate change would increase the risk of extreme bushfires in Australia. This warning was accurate. Scientists expect extreme fire weather will continue to become more frequent and severe without substantial and rapid action to reduce greenhouse gas emissions. Droughts that bring dust storms are also predicted to be more severe. Increased heat can cause more ozone pollution, which is already a threat to health, especially in Western Sydney. Where is the evidence that NSW has a plan to deal with these increased risks to its stated goals of ‘healthy environments’ for all? 

There is a statement in the strategy that urban growth is a risk to air quality but no explanation of how this link operates. There are statements about setting standards in planning that will improve air quality but no mention of how this fits with urban developments that are encroaching on the edges of the Sydney basin and Blue Mountains, a huge 24 hour airport and major road corridors planned the ten year period 2021 -2030 spanned by the Draft Strategy.  

The NSW Parliamentary Committee into the health impacts of poor air quality from bushfires and drought found that “due to the geographical and physical nature of Sydney, residents of Greater Western Sydney are exposed to much higher levels of air pollution than those in other parts of Sydney.” 

There is no sense in the Draft plan that the NSW government has plans to tackle the inequitable geographical impacts of air quality. This is of particular concern in a context in which there is  pressure on NSW  planners to make fast decisions driven by short term economic rather than health considerations. 

## No strategy to make national standards and exceedances more meaningful and comprehensive

“The NSW Government has a strong and established policy and regulatory framework that has achieved significant air quality improvements over recent decades. At the same time, however, national air quality standards continue to be exceeded (called ‘exceedances’) in cities and regions around the State.” 

Victoria has set higher air quality standards than NSW. There is no evidence in the strategy of a plan to lower the PM 2.5 standard to 7µg/m3.   This is important given that researchers have found that there is no lower limit of PM 2.5 that does have negative impacts on health. 

However even current standards are lowered, there are  no consequences for failure.  National Standards should be more than worthy goals. The draft strategy makes no proposals about how the government intends to tackle this issue.

## Improved policies will not necessarily deliver improved practice on the ground

**“The NSW Government is committed to continue building knowledge and expertise to update our policies to improve air quality and protect communities”.** 

Improving policies may be a welcome state but updating policies without action or follow through on the ground means little. Instead, policies can be a cover for lack of concrete plans. For example there are policies that landfills are supposed to be operated safely. 

## Case Study - Erskine Park Landfill - March/April 2021

Since at least March 2021, thousands of residents in Western Sydney have been exposed to foul sulphur dioxide odours for a major local landfill and perhaps other sources. Community feedback from Minchenbury and Erskine Park is that while the odours are particularly bad at the moment, smells which impact on quality of life and health are not at all unusual. Many complaints have been made previously. After months of receiving no information about the cause of the odours, residents finally decided to approach the media, which took up the story. Two days later, the NSW EPA issued a clean-up notice that required company Bingo to fix leachate pipes and take other steps to correct the issue.  While the EPA had been carrying out inspections in the area, why did it take so long to identify a likely cause of the problem?  During this period residents experienced headaches, feelings of nausea and other health impacts. It prevented residents exercising in their area and sleeping well. This same company has previously been fined for exceeding landfill limits. But last year, it was granted approval to increase its daily input into its Western Sydney and reported to the ASX that this would allow it to increase its profits. 

At the very least, this example  indicates a weakness in regulation of the landfill industry. There needs to be more information provided about how the strategy will actually improve the application and enforcement of EPA policies. This community is also one that is affected by ozone pollution, burn-off smoke and in some instances road pollution and is being confronted by the threat of unwanted waste to energy incinerators. 

The NSW community needs a strategy that demonstrates how it will tackle current inequities in relation to air quality experienced by residents in Western Sydney, the Hunter Valley, remote rural communities and vulnerable groups in the city. 

## Support for Environmental Justice Australia submission and People’s Action Plans

Environmental Justice Australia (EJA) has worked for many years on behalf of communities  to tackle  the impacts of coal pollution. In response to the plan, it has written, “Instead of providing a strategy with firm commitments and measurable deadlines to protect the community from toxic air pollution, the draft Clean Air Strategy merely summarises what the government already does (which we know isn’t enough).”

The EJA’s response is to develop its own People’s Action Plan. CEM supports the principles of the People’s Action Plan:

* Increasing air quality monitoring and access to information about air pollution 
* Reducing coal-fired power station pollution with best practice control standards 
* Reducing vehicle pollution, with a focus on vehicle pollution hotspots 
* Phasing out wood-burning heaters 
* Legislating health-based ambient air quality standards. 

Just as  important as the detail of the plan is the concept that communities should be actively involved in planning decisions including those about air quality,  Rather than a bureaucratic statement of goals and policies, CEM supports planning that begins with involving communities  in identifying issue with the assistance of technical expertise, at the outset, rather than seeking  feedback in a narrow period, with no guarantee of any response of any kind.  

## Lack of coordination between government departments and decision-makers

**“The NSW Government applies a whole-of-government approach to improving air quality, working with other jurisdictions, NSW agencies and also with industry, local government and community stakeholders.” (p. 2 of Draft Strategy).** 

A ‘whole-of.government’ approach suggests that departments and agencies will work together to achieve the goals of the lowest pollution levels possible. CEM’s experience is that air quality is not always prioritised and that EPA is not always empowered to carry out its own advice on air quality. For example:

* The NSW Department into the Impact of Westconnex found that the health of residents in St Peters and elsewhere had been impacted by poor air quality. When residents met with NSW Health they were told that there was little the department could do because they do not do local health studies and prefer a broader epidemiological approach. Given the localised impacts of air quality, this seems surprising and suggests there is a gap in the NSW government approach to the problem.
* When major infrastructure projects are being considered by DPIE, the EPA and NSW Health are invited to make submissions. However, they are often given little time to assess detailed responses before approval is given. [The EPA recommended against approval of the WestConnex Stage 3 until there was more detail provided of environmental impacts](https://www.wendybacon.com/2017/nsw-environmental-protection-authority-rejects-westconnex-eis/), but its views were overridden and the project was approved. 
* DPIE used Environmental Impact Statements prepared by private contractors to approve projects. There are no follow up evaluations by the EPA or NSW Health to assess the accuracy of the predictions of air quality that have been based on recycled out-of-data. This is a major flaw in the system.
* Local Councils have hired independent experts who have recommended against approval of projects including on the basis of air quality and their views have been overridden. In these cases, concerns about local air quality and impacts are overridden by prior political decisions to proceed with projects. When residents have raised concerns about high ambient air quality levels around the projects, they have been told that Transurban is only concerned with the impact of the tunnels not the impact of surface traffic. 
* The NSW Education Department refused to assist parents concerned about the failure of WestConnex and its air quality contractor Ecotech to supply them with data from an air quality monitor on school grounds. In later reports posted months later on the corporate website, it was revealed that there had been some daily exceedances in the monitor at the school.  On these days, there had been  no alerts or steps taken to protect children in the playground. It is hard to see how such secrecy could be consistent with a ‘whole of government’ approach.

## Insufficient attention to monitoring and research into roadside pollution

There is insufficient consideration in the Draft Strategy for monitoring roadside pollution. Why is there only one roadside monitor in Sydney? What plans does the government have to work with communities to enhance the information about roadside pollution, especially near where many people move during busy periods?

It is widely understood that pollution levels are higher beside busy roads. In 2019, the [University of Toronto did a pilot study](https://www.socaar.utoronto.ca/wp-content/uploads/2019/10/SOCAAR-Near-Road-Air-Pollution-Pilot-Study-Summary-Report-Fall-2019-web-Final.pdf) that showed the despite improvements in vehicle emissions, heavy trucks on urban roads, increased traffic and increasing number of people living near major roads do lead to roadside pollution being a significant issue. 

Independent health experts, researchers and communities have long argued that there should be more roadside monitoring in Sydney. There is no clear explanation for why this has not occurred. At the same time, there has been an explosion of construction in Sydney near major roads. schools and childcare centres continue to operate near heavy traffic, although it is known that young children are more vulnerable to higher levels of PM 2.5. 

There is no clear plan for more roadside monitoring in the 2021-30 strategy. This is despite the fact that Doctors Environment Australia have publicly advocated for roadside monitoring in key hotspots that have likely high population exposure. 

CEM agrees with Councillor Jess Miller from the City of Sydney Council’s used her submission to the recent Parliamentary Inquiry into Air Quality to call for more monitoring  at 'street level' from numerous sites within local communities, so that local government is better able to limit and respond to air pollution: There is an over reliance on background ambient air quality measurement which, although important, does not provide an indication of what levels of particulate matter is being experienced at different times of the day. Without roadside monitoring and  monitoring near massive construction sites, communities understandably lack faith assurances that risks are minimal.

As a result of approval requirements, there are currently about 20 Australian Standard Ecotech monitors being operated in Sydney as part of the opening of Transurban projects. Some of these are fairly close to busy roads. Why is there no plan to use this data to gain a better understanding of air quality in Sydney?  In preliminary analysis, CEM members have found ( not surprisingly) that levels are higher during peak periods. 

For example, there are eight surface monitors operating along the route of Transurban M8 in Sydney. At 7 am on a Monday morning in April 2021, one of these monitors shows significantly higher levels of PM 2.5  than the others. This is the monitor on an empty block of land on the busy intersection near the corner of Princess Highway and Campbell Street. There is a child care centre within a few metres of this monitor and St Peter public school next to that. The monitor shows 17.2 µg/m3 PM 2.5. This is 7 µg/m3 higher than the next monitor in Canal Road St Peters. Campbell Street monitor has the highest PM 10 of 25.6 µg/m3.

CEM’s point is not that one example illustrates a problem but that this data is being wasted and should be incorporated into the NSW government strategies for research and to provide more accurate data on which approvals for major projects can be given.
 
## A possible role for low-cost monitors in investigating roadside pollution

More evidence can also be gathered from low cost monitors such as the ones CEM is using. 

We chose Luftdaten monitors because of their low cost, the network’s commitment to open source methodologies and their wide use in a number of countries. We are aware from preliminary international research and our testing in Sydney that our monitors calibrate very well with each other but tend to measure lower levels in most conditions than the far more expensive Australian standard monitors that are used by the DPIE and Transurban. We consider them to be most useful for PM 2.5 

From our research so far, we are confident that the Luftdaten monitors allow users to compare air quality at their own monitor with other monitors elsewhere and to observe movements over time. For example because of their measurement at 150 second intervals , some residents found the monitors useful during the bushfires for evaluating when conditions were becoming healthier to exercise. It became clear to those observing the monitors during this period that smoke haze moves around and that even an hourly average level at a DPIE monitor may not provide evidence of when it is safe to exercise eight kilometres away.  Luftdaten maps provide public access to real-time pollution levels and short and long term trends at many locations.  We also believe that they can be a useful community, school and tertiary education tool. 

These monitors could  also be used in research into roadside monitoring. CEM’s own preliminary observations are that monitors near busy roads do regularly show significantly higher levels. Individual maps ( also publicly available) for each monitor allow residents to see pollution peaks and how pollution is tracking over time. So far we can provide no firm research analysis but hope to conduct more rigorous analysis. Community low cost monitoring that is open source can also provide a valuable source for research organisations. 

The strategy alludes to low cost censoring but does not indicate what plans the NSW government has to work with the community to use these fast-developing low cost technologies in the interests of public health.

## Lack of strategy for reducing impact of poor air quality on workers in NSW 

The recent NSW parliamentary Inquiry explored the impact poor air quality can have on those working outdoors, and potential protective measures that could be adopted in an employment context. This is an important issue which is not treated seriously in the Strategy document.

For example “the Australian Medical Association (NSW) was concerned by the 'uneven application of current occupational and health safety regulations', particularly given that workers in certain industries and occupations, particularly outdoor workers, are at heightened risk of adverse health outcomes during bushfires and periods of drought".

Both the Australian Medical Association (NSW) and the Australian Services Union NSW & ACT (Services) Branch, called for the introduction of health monitoring and assessments of outdoor workers for occupational illnesses related to poor air quality. The Australian Medical Association (NSW) stated this would 'facilitate targeted preventative measures'. (Health impacts of exposure to poor levels of air quality resulting from bushfires and drought report, page 52.)

CEM members and supporters have also observed that workers on NSW’s largest infrastructure projects have been seen working in hazardous conditions on days of extreme heat, dust and poor air quality. Many of these workers are contractors with no union protection. 

As Westconnex own monthly reports show,  many WestConnex workers on the St Peters construction sites were exposed to many daily exceedances of PM 10 during 2019 before the bushfire season even began. There seems to be no surveillance of conditions on construction sites  by the EPA. Corporate monthly reports do not appear to have been reviewed and even when raised by community representatives on WestConnex consultative committees did not receive an adequate response from the government, 

We support the submissions of the professionals, unions and community organisations that called for better strategies to protect workers from poor air quality. If NSW is to have an adequate air quality strategy, it must extend to ensuring all private corporations are accountable to NSW regulators. Current gaps in accountability for major construction projects must be identified and action taken to fill them.

## Insufficient evidence of recognition of need to improve relationship including communication with local communities

There is no indication in the draft strategy of how the NSW government plans to become more responsive to local communities. In 2017, residents in St Peters were exposed to months of toxic odours from the disturbed landfill which became the site of a WestConnex interchange. Hundreds of complaints were lodged and although action was eventually taken against the company, the delays were distressing and had health impacts  If delays in addressing issues are due to inadequate resourcing these should be addressed.

The draft states that the EPA will continue to use a best practice approach - but the communities observation is that there is a long way to go before the EPA’s approach to problems in local communities could be called ‘best practice’. Ite the experience in St Peters and Minchenbury - it takes too long - fast response to identify problems 

## Lack of consideration of construction impacts from major projects.

Despite concerns expressed by communities and Councils, the DPIE has continued to assert in assessments of major road infrastructure projects that impacts of construction on communities will not be ‘significant’ and therefore do not and require any quantification despite the recommendation of independent experts hired by Councils.  

After the projects are completed, there appears to be no follow-up evaluation of the EIS on which approvals were based even when the experience of communities on the ground is that the impacts were significant and exceeded predictions.

NSW Planning Department staff are even prepared to override the advice of the EPA itself. For example, the NSW EPA advised that there needed to be more evidence of the nature of  3 the impacts of the Stage 3 impacts of WestConnex. The Department ignored this advice and approved the project. 

In the case of the Stage 2 WestConnex, there were many complaints from demolition and construction, few  of which were satisfactorily or promptly resolved. Eventually, the odours from the site were so bad that residents were informed by doctors that their health was being impacted. It is alarming that although the EPA itself was concerned there was no way under Planning legislation that it could issue a stop work order. This power had been removed by little publicised legal changes. Eventually the company responsible was prosecuted for failing to control emissions and pleaded guilty but this did nothing to alleviate the problem at the time it was occuring.

In 2019, Wendy Bacon and Luke Bacon (member of CEM) completed an investigation into particulate pollution in St Peters. The  report was sparked by concerns about air pollution expressed by residents and parents of children at St Peters Public School, which was close to the construction site and nearby Princes Highway. It focused on a period since 2015.  

Their analysis of DPIE air quality monitoring results and the results of monitoring by WestConnex ( also Australian standard monitors) led them to conclude:

* Between , levels of particulate matter pollution at St Peters Primary School have been significantly higher than those recorded at nearby monitors managed by the then NSW Office of Environment and Heritage (OEH).
* In 70% of months, St Peters average PM 10 levels were higher than any recorded at OEH monitors in the Sydney basin.
* In 2018, the PM 2.5 average levels were higher than any OEH monitor up until September after which no reports had been published. 
* WestConnex construction and surface road emissions are likely to have contributed to these elevated levels.
* The Sydney Motorway Corporation failed to fulfil promises to supply information about air quality to the community. 
* Environmental Impact Statement air quality assumptions and predictions for annual averages and daily exceedances were exceeded. There was no avenue for this gap in accountability to be addressed.

WestConnex audit  reports published much later showed that there had been scores of daily exceedances of PM 10 at two monitors in St Peters during 2019, including before the bushfire season began. Community representatives raised this at a  consultative committee but received no adequate response. There is a clear gap in accountability for the impacts of construction projects. 

Any serious NSW Clean Air Strategy needs to fill gaps in accountability of large private construction companies and the toll road giant Transurban for their impacts on public health.. It is quite unfair and unsafe, to expose some communities to years of air quality that not only exceeds Australian national standards but also at times the World Health Organisation standard of 10 PM 2.5. With more projects in the pipeline, there needs to be an evaluation of the approval and regulatory arrangements for these road projects.  

# Conclusion

Looking at world map’s of pollution it is apparent at most times that Australia’s air quality is good compared to many other places in the world. However the 2019-20 bushfires exposed communities to very poor air quality for months. It has been predicted that 420 people died from smoke related effects. Even with a huge effort from the global community to rapidly reduce emissions, scientists predict we can expect more of this in the future. The bushfires were both a wake up call and a sign of the future.

But even in a year without a bushfire crisis, 2600-4800 people in Australia die as a result of exposure to toxic air pollution, at an annual health cost of $24 billion. The most recent analysis of health impacts caused by coal-fired power stations in Australia found that they contribute to 845 babies being born with low birth-weight, 14,434 children with asthma, and 785 premature deaths each year, at a cost of $2.4 billion dollars to the economy. Vulnerable people are hit hard by poor air quality – low-income communities, the elderly, people with chronic diseases, children, pregnant women and unborn babies.

Every year, more research is released pointing to even more previously unknown health impacts of even low levels of particulate matter. 

With an incomplete picture of current levels in NSW, no clear plans to fix deficiencies in controlling emissions, worsening air quality in 2019, some plans that could increase pollution, the need for a stronger strategy is clear.
