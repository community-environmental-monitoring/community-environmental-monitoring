---
layout: layouts/resource.njk
showHomeLink: true
postTitle: CEM briefing note on Bingo Industries licence changes
description: 
date: 2021-10-22
---

Earlier this year, residents living near the Bingo Industries landfill site at Eastern Creek [experienced months](https://www.wendybacon.com/2021/EPA-warned-Bingo-of-rain-danger-and-an-unexplained-fire) of foul smelling odours.

The site is operated under [a licence (13426)](https://app.epa.nsw.gov.au/prpoeoapp/Detail.aspx?instid=13426&id=13426&option=licence&searchrange=licence&range=POEO%20licence&prp=no&status=Issued) granted by the NSW Environmental Protection Authority. The licence is held in the name of Dial-A-Dump Pty Ltd and was originally granted in 2012. In 2019, Dial-a-Dump became a subsidiary of Bingo Industries. Bingo was acquired by Macquarie Infrastructure and Real Assets in August 2021.

The Bingo Eastern Creek landfill is licenced to accept only 'general solid waste' which is 'non-putrescible'. Non-putrescible wastes are solid wastes that do not readily undergo chemical or biological breakdown under conditions that produce odours.

Between April and July 2021, the NSW EPA received more than 750 complaints of foul-smelling odours in Minchinbury and other nearby suburbs. It is [an offence under the NSW Protection of the Environment Operations Act](https://legislation.nsw.gov.au/view/whole/html/inforce/current/act-1997-156#sec.129) for a person operating a licensed landfill to emit odours.

In April, the EPA ordered Bingo to take steps to eliminate the odours but the odours and the complaints continued. In May, the EPA issued temporary changes to the licence which required further steps to be taken, including the installation of gas wells to burn gases emitted from the landfill.  

On September 11, the NSW EPA issued an updated  licence with new conditions. From a lay perspective the changes seemed complex. For this reason, CEM research director and environmental scientist Charlie Pierce has prepared the following briefing note for the community. It reviews the changes and their significance.

## Bingo Landfill Licence 13426 Changes from the 7 May version to the 10 Sept 2021 version

Most of the licence changes are related to limiting the amount of water that can enter the landfill. The hydrogen sulphide odours were caused by too much rainwater infiltrating and contacting high carbon content waste under anaerobic conditions with [sulphur-reducing bacteria](https://en.wikipedia.org/wiki/Sulfur-reducing_bacteria) present. In these conditions, sulphur-reducing bacteria releases strong smelling sulphide into the air. Controlling water is one means to reduce the severity of future events. The other control would be to eliminate high amounts of organic waste but, it seems, this is too big an issue for the EPA to address.

### Changes and their significance

1. **A2.1 Name Change from Genesis Facility on Honeycomb Drive to Bingo Eastern Creek Landfill on 1 Kangaroo Avenue.**

   **Significance:** Nil

   * * *

2. **Condition P1.2 has been varied to include the reference "EPA reference No DOC21/723887" to the Monitoring Points.** Adding these words to each monitoring point was the only change. No points added or subtracted from previous licence.

   **Significance:** Nil.

   * * *

3. **Condition L3.7 has been added -- Waste types not permitted to be accepted at the Premises.** Specifically: The Licensee must not cause, permit or allow the following waste to be received or applied to land at the Premises: 

   a) Mixed waste organic outputs (MWOO) 

   b) Waste resulting from flood clean up works or activities 

   c) Any other waste that emits or has the potential to emit offensive odours.

   **Significance:** Officially prohibiting some wastes with odour potential could reduce odour in the future.

   * * *

4. **Condition O3.1 has been replaced by a new condition -- dust.** The new condition is: "All operations and activities occurring at the premises must be carried out in a manner that will minimise dust emissions on the premises and prevent the emission of dust from the premises". The section in italics has been added.

   **Significance:** This makes emitting dust off site a violation. This is a strict condition because landfills do emit dust and it is doubtful that Bingo will even try to comply. They report from dust gauges at the perimeter. Even though the Bingo Eastern Creek landfill mostly complies with the 4 gram per m3 per month limit, they do emit dust from the premises.

   * * *

5. **Condition O4.4 has been replaced by a new condition -- sediment tracking.** The old requirement was: "The Licensee must minimise the tracking of waste and mud by vehicles leaving the premises." The new licence states: "The licensee must ensure no material, including sediment, is tracked from the premises.

   **Significance:** This is a stricter control. Nothing is allowed to contaminate the roads from the vehicles leaving the site.

   * * *

6. **Condition O5.3 has been varied -- landfilled waste.** Specifically: "The licensee must not exhume and/or expose any landfilled waste at the premises unless written approval is given by the EPA". Previously it stated the same thing without the qualifier 'and/or expose'.

   **Significance:** Further tightening management practice. When waste is exposed the likelihood of odour from the waste and water infiltration that could produce more leachate and hydrogen sulphide is enhanced.

   * * *

7. **Condition O5.4 states it has been varied, but CEM could not find any difference between the current and the previous requirement.** It states "Cover material must be: 

   a) Daily Cover

   Daily Cover must be either:

   i) virgin excavated natural material, or

   ii) approved alternative daily cover.

   Daily cover material must be applied to a minimum depth of 15 centimetres over all exposed landfilled waste prior to ceasing operations at the end of each day.

   b) Intermediate Cover must be virgin excavated natural material.

   Intermediate cover material must be applied to a depth of 30 centimetres over surfaces of the landfilled waste at the premises which are to be exposed for more than 90 days.

   c) Cover material stockpile

   At least two weeks' cover material must be available at the premises under all weather conditions. This material may be won on site, or alternatively a cover stockpile must be maintained adjacent to the tip face."

   Previously, it said the same thing. 

   **Significance:** These requirements are straight out of the EPA's NSW Landfill Guidelines (2016). I guess this was inserted at some time in the past because Bingo failed to comply with cover requirements which would lead to greater water infiltration and leachate production.

   * * *

8. **Conditions O5.5 and O5.6 have been deleted -- Alternative Daily Cover.** Bingo was allowed to use Concover as an alternate daily cover material and is no longer allowed to do so. 

   **Significance:** Daily cover is a significant operating expense  for landfill operators and it can use up licenced volume which reduces the amount of waste that may be disposed. Landfill operators often seek approval to use Alternative Daily Cover as a means to reducing this cost and minimised the volume of lost space.This was probably revoked because Bingo was not meeting the performance requirements for daily cover, i.e., reducing infiltration and vector (rodents and insects) attraction.

   * * *

9. **New condition** "Until the updated filling plan, pursuant to Condition U1.4, is approved by the EPA and implemented, the Licensee must ensure that the size of the active waste tipping face or tipping faces (being the total area of the landfill surface that has uncovered waste) does not exceed 1000 square metres at any time.

   **Significance:** A 1000 m2 tipping face (where Bingo puts waste) is generous. Limiting the tipping face reduces the amount of water infiltration that is possible. It seems that Bingo must have had a larger tipping face than 1000 m2 in the past which prompted this specific requirement.

   * * *

10. **New condition O5.6 has been added - Uncovered landfill surface by intermediate cover.** It states "Until the updated filling plan, pursuant to Condition U1.4, is approved by the EPA and implemented, the Licensee must ensure that the size of the total area of the landfill surface that is not covered by intermediate cover does not exceed 4000 square metres at any time.

    **Significance:** Intermediate cover is better at controlling infiltration than daily cover. Limiting the area that only has daily cover to 4000 m2 will reduce infiltration and leachate production.

    * * *

11. **Condition O5.14 has been varied -- Leachate level.** It states: 'The licensee must ensure that the leachate levels within the landfill are maintained below 0m Australian Height Datum (AHD)." Previously, it stated that "The licensee must ensure that the leachate levels within the landfill below RL 25m AHD are maintained at  least 5m below the minimum elevation of the waste surface.

    **Significance:** This requirement lowers the allowable leachate level by 25 metres. The lower the level of leachate in the waste the greater the margin of safety from infiltration events.

    * * *

12. **Condition O6.7 has been added -- Stormwater management at the landfill surface.** The new requirement is: "From 30 September 2021, the perimeter of the areas where waste has been or is being landfilled must be contoured to prevent stormwater running onto these surfaces from all storm events less than or equal to a 1 in 10 year 24 hour duration rainfall event annual rainfall average recurrence intensity (ARI)".

    **Significance:** This requirement is common sense engineering that modern landfills undertake as a matter of course. Bingo may not have been diligently contouring the land to prevent stormwater from entering the active face. So, it must have been necessary to explicitly add this activity to the licence to ensure intelligent surface engineering.

    * * *

13. **Condition O6.8 has been added -- Surface water management at the landfill surface.** This addition was: "From 30 September 2021, surface waters must be diverted away from any area where waste is being or has been landfilled."

    **Significance:** Common sense engineering for all landfills to keep water out of waste.

    * * *

14. **Condition M7.4 has been replaced -- Monitor leachate level.** It states "The licensee must monitor and record, weekly, the height of the leachate relative to the Australian Height Datum at the leachate riser in the landfill." Leachate is formed when rain water filters through wastes placed in a landfill. When this liquid comes in contact with buried wastes, it leaches, or draws out, chemicals or constituents from those wastes. A leachate riser is the name given to the pipe that is used to pump the leachate to the surface. It previously stated "The Licensee must monitor and record, weekly, the height of the leachate relative to the Australian Height Datum at EPA Points 31 and 32".

    **Significance**: this change seems to be a simplification as monitoring point 31 no longer exists.

    * * *

15. **Condition R2.7 has been added -- Notification of methane exceedance.** This requirement is: "The licensee must notify the EPA within 24 hours in accordance with condition R2.1 if any surface or enclosed space landfill gas monitoring undertaken at the premises detects methane concentrations above 1.25% , and

    a) increase the frequency of monitoring to daily, until the EPA determines otherwise, and
   
    b) submit a written assessment to the EPA within 14 days of the incident becoming known to the licensee detailing the emissions and the management controls implemented (or proposed to be implemented) to prevent further emissions.
   
    **Significance:** This requirement is very lenient. The NSW EPA Landfill Guidelines state that action must be taken if the methane detected at the surface was greater than 500 ppm or 0.05 %. It is not clear why the trigger level in the licence has been set so high for a non-putrescible landfill known to be generating 1,800 m3 of methane per hour.

    * * *

16. **Condition U1 has been added under Pollution Studies and Reduction Program.** It states: "During April to June 2021, the EPA identified offensive odours being emitted from the premises and also received a large number of community odour complaints.

    This condition requires investigations and reporting to be undertaken by thelicensee to implement improved management practices and address odorous emissions.

    Aim

    To investigate, develop, & implement works to minimise / manage landfill gases and eliminate off-site odours, as well as provide information to the community on these works, and on future incidents or events that could lead to offensive odour emissions from the Premises".

    **Significance:** This requirement lacks specificity; it is a general direction to the landfill operators. It has always been illegal to emit odours offsite. The Bingo Landfill has a history of not taking any action unless there is a direct order in the licence. Based on past practices, it can be unlikely that Bingo will do anything until explicitly told to do so. A better condition would be that Bingo must stop operating whenever it cannot control offsite odours.

    * * *

17. **E4 and E5 were deleted.** These conditions allowed for alternative daily cover and emergency requirements placed on Bingo in May as temporary controls during the height of the 2021 odour incident.

    * * *

18. **U1.3 is added which requires an updating of Bingo's Community Engagement Strategy.** The requirements are:

    "(a) The licensee must update its Community Engagement Strategy to keep the community informed of the progress of works to rectify landfill odours, the systems in place to manage odour risks, contemporary system upsets or scheduled maintenance that might increase the risk of odours, and any other environmental matters at the premises.
    
    (b) The information presented in the strategy, must include, but may not be limited to;
    
    - a description of the cause of the odours,
    
    - proposed short-term and longer-term odour management works or options,
    
    - the progress to date on these works and options, and
    
    - contact details for community members to make a report or contact a representative of the licensee. The strategy could include delivery of information through mediums such as; the licensee's website, targeted letterbox drops, articles in printed news media or social media sites.
    
    (c) A copy of the Community Engagement Strategy must be provided to the EPA at least one (1) week before the due date.
    
    (d) The Community Engagement Strategy must be placed on the Licensee's website and implemented by the due date. Due Date: 22 September 2021.
    
    **Significance:** The idea is to have the Bingo Landfill consider its neighbours before doing environmental and community harm. Today is 11 October and there isn't any strategy on the Licensee's website (<https://www.bingoindustries.com.au/Policies/eastern-creek-policies-and-reporting>). Being 3.5 weeks late is indicative of how seriously Bingo takes its responsibilities for community consultation.

    * * *

## What is needed?

To address the issue of non-putrescible landfills with high organic content would require the EPA to set specific limits on the amount of organic matter (all types) permitted to be landfilled. It would seem that this is too big a task for the NSW Environment Protection Authority.

Bingo is not the kind of company that will do a proper community consultation. They just don't care.
